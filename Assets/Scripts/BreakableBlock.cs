﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BreakableBlock : MonoBehaviour {

	public float integrity = 25f;
	public GameObject smash;

	// Use this for initialization
	void Start () {

	}

	// Update is called once per frame
	void Update () {
		if (integrity <= 0f) {
			Destroy (gameObject);

			if (smash != null) {
				GameObject smashInstance = Instantiate (smash, transform.position, transform.rotation);
				//smashInstance.transform.localScale = transform.localScale;
			}
		}
	}

	void OnCollisionEnter (Collision col) {
		integrity -= col.relativeVelocity.magnitude;
	}
}
