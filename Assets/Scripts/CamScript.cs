﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CamScript : MonoBehaviour {

	Vector3 startPos;

	bool freeLook = true;

	float frustrumHeight;
	float frustrumWidth;

	// Use this for initialization
	void Start () {
		startPos = transform.position;
		calculateStartPos ();
		transform.position = startPos;
	}
	
	// Update is called once per frame
	void FixedUpdate () {

		if (GameManager.gm.gameState == GameManager.gameStates.playing && Input.GetAxis("Mouse ScrollWheel") != 0f) {
			calculateFrustrumDimensions ();
			float oldLeft = transform.position.x + (frustrumWidth/2);

			transform.position += ((Vector3.forward * Input.GetAxis("Mouse ScrollWheel")) / Time.deltaTime);

			if (transform.position.z > -50f) transform.position = new Vector3(transform.position.x, transform.position.y, -50f);
			else if (transform.position.z < -200f) transform.position = new Vector3(transform.position.x, transform.position.y, -200f);

			calculateFrustrumDimensions();
			transform.position = new Vector3 (oldLeft - (frustrumWidth/2), transform.position.y, transform.position.z);
		}

		calculateStartPos ();

		if (GameManager.gm.gameState == GameManager.gameStates.playing) {
			if (GameManager.gm.currentProjectile != null &&
			   GameManager.gm.slingState == GameManager.slingStates.fired && GameManager.gm.currentProjectile.transform.position.x > startPos.x) {
				float newPos = Mathf.Lerp (transform.position.x, GameManager.gm.currentProjectile.transform.position.x, 0.2f);
				transform.position = new Vector3 (newPos, transform.position.y, transform.position.z);
				freeLook = false;
			} else if (GameManager.gm.slingState == GameManager.slingStates.looking) {
				if (freeLook == false) { // Return to normal
					float newPos = Mathf.Lerp (transform.position.x, startPos.x, 0.1f);
					transform.position = new Vector3 (newPos, transform.position.y, transform.position.z);
					if (transform.position.x > startPos.x - 0.001f && transform.position.x < startPos.x + 0.001f)
						freeLook = true;
				} else if (freeLook == true) {
					if (Input.mousePosition.x < Screen.width / 5)
						transform.position -= new Vector3 (0.5f, 0f, 0f);
					else if (Input.mousePosition.x > Screen.width - (Screen.width / 5))
						transform.position += new Vector3 (0.5f, 0f, 0f);

					if (transform.position.x < startPos.x)
						transform.position = new Vector3 (startPos.x, transform.position.y, transform.position.z);
				}
			}

			transform.position = new Vector3 (transform.position.x, startPos.y, transform.position.z);
		}

	}

	void calculateStartPos () {
		calculateFrustrumDimensions ();

		var startX = (GameManager.gm.slingshot.transform.position.x - frustrumWidth/2) - 5f;
		var startY = (GameManager.gm.slingshot.transform.position.y - frustrumHeight/2) - 3f;

		startPos = new Vector3(startX, startY, transform.position.z);
	}

	void calculateFrustrumDimensions() {
		frustrumHeight = 2.0f * (transform.position.z) * Mathf.Tan (GameManager.gm.getAcam().fieldOfView * 0.5f * Mathf.Deg2Rad);
		frustrumWidth = frustrumHeight * GameManager.gm.getAcam().aspect;
	}

	public void setFreeLook(bool f) {
		freeLook = f;
	}
}
