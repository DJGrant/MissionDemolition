﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAndStretch : MonoBehaviour {
	public Transform target;
	public Vector3 targetOffset;
	float regularScale;

	// Use this for initialization
	void Start () {
		regularScale = transform.localScale.z;
	}

	// Update is called once per frame
	void LateUpdate () {
		//Vector3 targetPoint = (target.position + targetOffset);
		Vector3 targetPoint = (target.position + (target.rotation * targetOffset));

		transform.LookAt (targetPoint);

		transform.localScale = new Vector3(regularScale, regularScale, (targetPoint - transform.position).magnitude * regularScale);
	}
}
