﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour {

	public static GameManager gm;

	public GameObject slingshot;
	public GameObject camera;

	public GameObject winCanvas;
	public GameObject failCanvas;

	public string nextScene;

	public enum slingStates {looking, aiming, fired};
	public enum gameStates {playing, complete, failed};
	public slingStates slingState = slingStates.looking;
	public gameStates gameState = gameStates.playing;

	[HideInInspector]
	public GameObject currentProjectile;

	// Use this for initialization
	void Awake () {
		if (gm == null)
			gm = GetComponent<GameManager> ();

		winCanvas.SetActive (false);
		failCanvas.SetActive (false);
	}
	
	// Update is called once per frame
	void Update () {
		switch (gameState) {
		case gameStates.complete:
			winCanvas.SetActive (true);
			failCanvas.SetActive (false);
			break;
		case gameStates.failed:
			winCanvas.SetActive (false);
			failCanvas.SetActive (true);
			break;
		}
	}

	public Camera getAcam() {
		return camera.transform.GetChild (0).gameObject.GetComponent<Camera>();
	}

	public void setCurrentProjectile(GameObject obj) {
		currentProjectile = obj;
	}

	public void ContinueToNextScene() {
		if (nextScene != "")
			SceneManager.LoadScene (nextScene);
		else
			Application.Quit();
	}

	public void ReloadScene() {
		SceneManager.LoadScene (SceneManager.GetActiveScene().name, LoadSceneMode.Single);
	}
}
