﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlingHolderControl : MonoBehaviour {

	GameObject slingshot;
	PlayerControl slingScript;

	float slingStretch;

	[HideInInspector]
	public Vector3 neutralPoint;

	Camera cam;


	public float stretchLimit = 4.5f;

	// Use this for initialization
	void Start () {
		slingshot = transform.parent.gameObject;
		slingScript = slingshot.GetComponent<PlayerControl> ();

		neutralPoint = slingshot.transform.position + new Vector3 (0f, slingshot.transform.localScale.y * 2.8f, 0f);

		cam = Camera.main;
	}
	
	// Update is called once per frame
	void FixedUpdate () {
		if (slingScript.aimingMode == true) { // If we've got the slingshot pulled back and are aiming...
			
			Vector3 newPos = cam.ScreenToWorldPoint (new Vector3 (Input.mousePosition.x, Input.mousePosition.y, -cam.transform.position.z));
			transform.position = new Vector3 (newPos.x, newPos.y, 0f);

			if ((Mathf.Abs ((transform.position - (neutralPoint)).magnitude)) > stretchLimit) { // Stretch limit
				transform.position = (neutralPoint + ((transform.position - neutralPoint).normalized * stretchLimit));
			}

			transform.LookAt (neutralPoint);

			if (transform.position.x > neutralPoint.x) // To avoid the weird-looking flip that happens when you cross the holder over to the front
				transform.RotateAround (transform.position, Vector3.up, 180f);

			if (transform.position.x > slingshot.transform.position.x - 0.5f) // Actually, Don't go in front
				transform.position = new Vector3 (slingshot.transform.position.x - 0.5f, transform.position.y, transform.position.z);

			//if (transform.position.y < slingshot.transform.position.y + 0.5f) // Don't go below the ground
				//transform.position = new Vector3 (transform.position.x, slingshot.transform.position.y + 0.5f, transform.position.z);

			slingStretch = Mathf.Clamp( ((neutralPoint.x - transform.position.x)/7f) + ((neutralPoint.y - transform.position.y)/4f), 0f, 1f);
		} else { // Or not
			Vector3 newPos = Vector3.Lerp (transform.position, neutralPoint, 0.1f);
			transform.position = new Vector3 (newPos.x, newPos.y, 0f);

			transform.rotation = Quaternion.Euler(new Vector3 (0f, 90f, 0f));

			slingStretch = 0f;
		}

		slingshot.GetComponent<Animator> ().SetFloat ("SlingStretch", slingStretch);
	}

	void OnMouseOver() {
		if (GameManager.gm.gameState == GameManager.gameStates.playing)
			transform.localScale = Vector3.Lerp(transform.localScale, new Vector3 (1f,1.5f,1f), 0.5f);
	}

	void OnMouseExit() {
		transform.localScale = Vector3.Lerp(transform.localScale, new Vector3 (1f,1f,1f), 0.5f);
	}

	void OnMouseDown() {
		if (GameManager.gm.currentProjectile != null) {
			if (GameManager.gm.currentProjectile.GetComponent<Projectile> ().hasBeenFired == false) {
				slingScript.aimingMode = true;
				GameManager.gm.slingState = GameManager.slingStates.aiming;
			}
		}
	}

	void OnMouseUp() {
		slingScript.aimingMode = false;
	}
}
