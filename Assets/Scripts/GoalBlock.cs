﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GoalBlock : MonoBehaviour {

	public GameObject sparkles;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void OnCollisionEnter (Collision col) {
		if (col.gameObject.tag.Equals ("Projectile")) {
			GameManager.gm.gameState = GameManager.gameStates.complete;

			GetComponent<Animator> ().SetTrigger ("isHit");

			if (sparkles != null)
				Instantiate (sparkles, transform.position, Quaternion.identity);
		}
	}
}
