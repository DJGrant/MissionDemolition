﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Projectile : MonoBehaviour {

	public float integrity = 50f;
	public GameObject destructEffect;

	[HideInInspector]
	public bool hasBeenFired = false;

	Vector3 lastPos;
	float destructoTimer = 5f; // So it won't lie around forever after being fired.

	// Use this for initialization
	void Start () {
		lastPos = transform.position;
	}
	
	// Update is called once per frame
	void Update () {
		if (destructoTimer <= 0f || transform.position.y < -10f)
			DestroyProjectile ();

		if (integrity <= 0f && hasBeenFired) {
			DestroyProjectile ();
		}

		if (Mathf.Abs (transform.position.magnitude - lastPos.magnitude) < 0.1f && hasBeenFired)
			destructoTimer -= Time.deltaTime;

		lastPos = transform.position;
	}

	void OnCollisionEnter (Collision col) {
		if (hasBeenFired)
			integrity -= col.relativeVelocity.magnitude;
	}

	void DestroyProjectile() {
		GameManager.gm.slingshot.GetComponent<PlayerControl> ().loadNextProjectile ();
		GameManager.gm.slingState = GameManager.slingStates.looking;

		if (destructEffect != null)
			Instantiate (destructEffect, transform.position, Quaternion.identity);

		Destroy (gameObject);
	}
}
