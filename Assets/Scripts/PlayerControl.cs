﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerControl : MonoBehaviour {

	//Rigidbody rb;
	GameObject holder;

	public GameObject[] projectiles;

	public float launchForce = 7.5f;

	GameObject[] arsenal;

	int arsenalIndex = 0;
	GameObject currentProjectile;

	[HideInInspector]
	public bool aimingMode = false;
	bool isLoaded = false;

	// Use this for initialization
	void Start () {
		//rb = GetComponent<Rigidbody> ();
		holder = transform.GetChild(1).gameObject;

		arsenal = new GameObject[projectiles.Length];

		for (int i = 0; i < projectiles.Length; i++) { // Instantiate projectiles
			arsenal [i] = Instantiate (projectiles[i], new Vector3(transform.position.x + (1f * (i+1)), transform.position.y + 2f, 1.5f), Quaternion.identity) as GameObject;
		}
		//Debug.Log ("Arsenal: " + arsenal[arsenalIndex]);
		currentProjectile = arsenal[arsenalIndex];
		GameManager.gm.setCurrentProjectile (currentProjectile);
	}
	
	// Update is called once per frame
	void Update () {

		if (aimingMode == true && arsenalIndex < arsenal.Length) { // Ready, aim...
			isLoaded = true;
			currentProjectile.GetComponent<Rigidbody> ().isKinematic = true;

			currentProjectile.transform.position = holder.transform.position;
			currentProjectile.transform.rotation = holder.transform.rotation;
		}

		else if (aimingMode == false && isLoaded == true) { // ...FIRE!
			isLoaded = false;
			GameManager.gm.slingState = GameManager.slingStates.fired;

			//currentProjectile.transform.position = holder.GetComponent<SlingHolderControl> ().neutralPoint;
			currentProjectile.GetComponent<Projectile>().hasBeenFired = true;
			currentProjectile.GetComponent<Rigidbody> ().isKinematic = false;
			currentProjectile.GetComponent<Rigidbody> ().AddForce ((holder.GetComponent<SlingHolderControl> ().neutralPoint - holder.transform.position) * launchForce * currentProjectile.GetComponent<Rigidbody>().mass, ForceMode.Impulse);
		}

		if (GameManager.gm.gameState == GameManager.gameStates.playing && arsenalIndex >= arsenal.Length) {
			GameManager.gm.gameState = GameManager.gameStates.failed;
		}
	}

	public void loadNextProjectile () {
		arsenalIndex++;
		if (arsenalIndex < arsenal.Length) {
			currentProjectile = arsenal [arsenalIndex];
			GameManager.gm.setCurrentProjectile (currentProjectile);
		}
	}
}
