﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AimAtThing : MonoBehaviour {

	public Transform target;
	public bool LockXZ;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		transform.LookAt (target.position);

		if (LockXZ == true)
			transform.eulerAngles = new Vector3 (0, transform.eulerAngles.y, 0); // Keep the thing from looking up and down or rolling side to side.
	}
}
